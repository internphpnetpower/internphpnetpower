<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\RouterInterface;

class BlogController extends AbstractController
{
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @Route("/blog", name="blog_list")
     * @param Request $request
     */
    public function list(Request $request)
    {
        // ...

        // generate a URL with no route arguments
        $signUpPage = $this->generateUrl('lucky_number', [
            'percent' => rand(0, 99),
        ]);


        try {
            $url = $this->router->generate('lucky_number', [
                'percent' => rand(0, 99),
            ]);
        } catch (RouteNotFoundException $e) {
            // the route is not defined...
        }

        $routeParameters = $request->attributes->get('_route_params');

        // use this to get all the available attributes (not only routing ones):
        $allAttributes = $request->attributes->all();

        return $this->render('lucky/number.html.twig', [
            'number' => $url,
        ]);
    }
}
